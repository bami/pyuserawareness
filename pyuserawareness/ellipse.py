# -*- coding: utf-8 -*-
"""
    pyuserawareness.ellipse
    ~~~~~~~~~~~~~~~~~~~~~~~

    :copyright: (c) by 2012 Oliver Schlatter <olivschl@student.ethz.ch>
    :license: BSD3, see LICENSE for more details.
    :description: paints ellise shaddow (of ball) on screen
"""

import sys
import math
from PyQt4 import QtGui, QtCore
from pykinecttracker.kinect import ViewpointTrackerKinect
from pykinecttracker.dummy import StaticViewpointTracker
###################################################

## Infos ########################
# Bildschirmaufloesung: 1440x900 Pixel  => 30.5cm = 1440 Pixel and 21cm = 950 
#                                         99.9 DPI => 39.33 DPcm
##Global Input###############################

Kugelfarbe = 'blue'
Hintergrundfarbe = 'green'

r = 20 # Radius der Kugel [mm]

pixelpitch_x = 200./56. # [pixel/mm]
pixelpitch_y = 200./56. # [pixel/mm]

offset = 0.  # Abstand zwischen Beruehrungs- und Darstellungsebene [mm]

xK = 0.0  #Viewpoint [mm]
yK = 0.0  #
zK = 400.0  # !> 2*r

xKinect = 45.  #Ortsvektor der Kinectkamera bezueglich Bildschirmkoordinatensys
yKinect = -80. #
zKinect = 0.  #

Kalibration = 'off' # 'on' / 'off'. 'on': Quadrat mit 200x200 Pixel wird gezeichnet
###################################################

class MovingEllipseWidget(QtGui.QWidget):
    """
    Moving ellipse widget

    position gathered from interaction point
    orientation garthered from viewpoint tracker
    """
    def __init__(self):
        
        super(MovingEllipseWidget, self).__init__()
        self.initUI()
        
    def initUI(self):      

        self.setWindowTitle('Ellipse')
        self.showMaximized()

    def paintEvent(self,event):

        qp = QtGui.QPainter()
        qp.begin(self)
        self.drawEllipsen(qp)
        qp.end()
        
    def drawEllipsen(self, qp):
        origin = QtCore.QPoint(0,0)        

        qp.setBrush(QtGui.QColor(Hintergrundfarbe).lighter(300))
        #qp.drawRect(0,0,3000,2000)
                
        if Kalibration == 'on':
            qp.drawRect(100,100,200,200)
        
        qp.translate(self.xEMP*pixelpitch_x,self.yEMP*pixelpitch_y)
        
        qp.setPen(QtGui.QColor('black'))   
        
        color = QtGui.QColor(self.farbe)
        color.setNamedColor(self.farbe)
        qp.setPen(color)
        
        qp.rotate(-self.alpha/3.141*180)                                    
                              
        qp.setBrush(QtGui.QColor(self.farbe))
        Grad = QtGui.QRadialGradient(0.0,0.0,self.L/2)
        Grad.setRadius(r*pixelpitch_x)
        Grad.setColorAt(0, QtGui.QColor('white'))
        Grad.setColorAt(0.3,QtGui.QColor(self.farbe).lighter(155))
        Grad.setColorAt(1, QtGui.QColor(self.farbe))
        qp.setBrush(Grad)
        
        qp.drawEllipse(origin,self.L/2.0*((math.cos(math.fabs(self.alpha))**2)*pixelpitch_x+(math.sin(math.fabs(self.alpha))**2)*pixelpitch_y),self.b*((math.cos(math.fabs(self.alpha))**2)*pixelpitch_y+(math.sin(math.fabs(self.alpha))**2)*pixelpitch_x))

    def mousePressEvent (self, event):
        self.xKlick,self.yKlick = event.pos().x(), event.pos().y()
        self.Ellipsengleichungen()
        self.update()

    def mouseMoveEvent (self, event):
        self.xKlick,self.yKlick = event.pos().x(), event.pos().y()
        self.Ellipsengleichungen()
        self.update()   
        
    def Ellipsengleichungen(self):
        
        self.r = r
        self.xKlick = self.xKlick/pixelpitch_x
        self.yKlick = self.yKlick/pixelpitch_y
        
        #self.xK = k.getViewpoint(0)[0]
        #self.yK = -k.getViewpoint(0)[1]
        #self.zK = k.getViewpoint(0)[2]
        
        self.xK = xK
        self.yK = yK
        self.zK = zK
        
        self.x = math.sqrt((self.xKlick-self.xK-xKinect)**2+(self.yKlick-self.yK-yKinect)**2)
        self.z = self.zK+zKinect
        
        print self.xKlick
        
        print self.x, self.z
        
        self.alpha = math.atan((self.yKlick-self.yK-yKinect)/(-self.xKlick+self.xK+xKinect))               
        
        self.farbe = Kugelfarbe
        
        # Ellipsengleichungen #
        Sqrt = math.sqrt
        Sin  = math.sin
        ArcSin = math.asin
        Cos  = math.cos
        Tan  = math.tan
        ArcTan = math.atan        
        
        Pi = 3.14159
        
        self.L=r*(Tan((Pi/2-ArcTan(self.x/(self.z-r))+ArcSin(r/(Sqrt(self.x**2+(self.z-r)**2))))/2)+Tan((Pi/2+ArcTan(self.x/(self.z-r))+ArcSin(r/(Sqrt(self.x**2+(self.z-r)**2))))/2))*(1+offset/self.z)
        self.k=r*(Tan((Pi/2-ArcTan(self.x/(self.z-r))+ArcSin(r/(Sqrt(self.x**2+(self.z-r)**2))))/2))
        self.e=(self.k-self.L/2)*(1-offset/self.z)-offset*self.x/self.z
        self.epsilon=self.x*self.z/(self.z-r)-self.x+self.k
        self.s=Sqrt(1-(self.epsilon/(self.L-self.epsilon)-1)**2)*r*Sin(Pi/2-ArcSin(r/Sqrt(self.x**2+(self.z-r)**2)))
        self.b=(Sqrt((self.L/2-self.k+self.x)**2+self.z**2)/(Sqrt(self.x**2+(self.z-self.r)**2))*self.s)*(1+offset/self.z)
                        
        if -self.xKlick+xK+xKinect > 0:
            self.xEMP = self.xKlick+self.e*Cos(self.alpha)
        else:
            self.xEMP = self.xKlick-self.e*Cos(self.alpha)        
        
        if -self.xKlick+xK+xKinect > 0:
            self.yEMP = self.yKlick-self.e*Sin(self.alpha)
        else:
            self.yEMP = self.yKlick+self.e*Sin(self.alpha)

        self.update()
        
   
def main():    
    global k 
    # kinect viewpoint tracker
    #k = ViewpointTrackerKinect(libraryPath="lib/libSkeletonTrackerKinect.so")
    # statuc viewpoint
    k = StaticViewpointTracker(position=[0,0,100])

    app = QtGui.QApplication(sys.argv)

    ex = MovingEllipseWidget()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
