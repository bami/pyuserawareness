# -*- coding: utf-8 -*-
"""
    pyuserawareness.cube
    ~~~~~~~~~~~~~~~~~~~~

    :copyright: (c) by 2012 Oliver Schlatter <olivschl@student.ethz.ch>
    :license: BSD3, see LICENSE for more details.
    :description: rotating cube
"""

import sys
from PyQt4 import QtGui, QtCore

###################################################

Wuerfelfarbe = 'green'

a = 200. # Kantenlaenge des Wuerfels [mm]

pixelpitch_x = 200./42. # [pixel/mm]
pixelpitch_y = 200./42. # [pixel/mm]

offset = 50.  # Abstand zwischen Beruehrungs- und Darstellungsebene [mm]

xK = 720.0  #Viewpoint [mm]
yK = 450.0  #
zK = 800.0  # !> 2*r

xKinect = 0  #Ortsvektor der Kinectkamera bezueglich Bildschirmkoordinatensys
yKinect = 0  #
zKinect = 0  #

###################################################

class CubeWidget(QtGui.QWidget):
    
    def __init__(self):
        
        super(CubeWidget, self).__init__()
        self.count = 0
        self.PList = [QtCore.QPointF(0,0),QtCore.QPointF(0,0),QtCore.QPointF(0,0),QtCore.QPointF(0,0)]
        self.Quadrant = 3

        self.initUI()
        
    def initUI(self):      

        self.setWindowTitle('Cube')
        self.showMaximized()

    def paintEvent(self,event):    

        qp = QtGui.QPainter()
        qp.begin(self)
        self.drawPolygon(qp)
        self.drawPolygon(qp,)
        qp.end()              
    
        
    def drawPolygon(self, qp):
        
        qp.setPen(QtGui.QColor(Wuerfelfarbe).darker(120))
        
        qp.setBrush(QtGui.QColor(Wuerfelfarbe))
        
        if self.Quadrant == 1:
            qp.drawPolygon(self.polygon3)
            qp.drawPolygon(self.polygon5)
            qp.drawPolygon(self.polygon1)
        if self.Quadrant == 2:
            qp.drawPolygon(self.polygon4)
            qp.drawPolygon(self.polygon5)
            qp.drawPolygon(self.polygon1)
        if self.Quadrant == 3:
            qp.drawPolygon(self.polygon2)
            qp.drawPolygon(self.polygon4)
            qp.drawPolygon(self.polygon1)
        if self.Quadrant == 4:
            qp.drawPolygon(self.polygon2)
            qp.drawPolygon(self.polygon3)
            qp.drawPolygon(self.polygon1)
            
    def mousePressEvent (self, event):       
        
        self.PList[self.count]=QtCore.QPointF(event.pos().x(),event.pos().y())
            
        self.Projektion()

        self.update()
        
    def mouseMoveEvent (self, event):
        
        self.PList[self.count]=QtCore.QPointF(event.pos().x(),event.pos().y())

        self.Projektion()

        self.update()

    def Projektion(self):
        
        Pkt = QtCore.QPointF(self.PList[0])-QtCore.QPointF((xK+xKinect-a/2),(yK+yKinect-a/2))
        
        if Pkt.x() > 0:
            if Pkt.y() < 0:
                self.Quadrant = 1
            else:
                self.Quadrant = 4
        else:
            if Pkt.y() < 0:
                self.Quadrant = 2
            else:
                self.Quadrant = 3
                
        self.PList[1]=QtCore.QPointF(self.PList[0])
        self.PList[1].setY(self.PList[0].y()+a)
        
        self.PList[2]=QtCore.QPointF(self.PList[0])
        self.PList[2].setX(self.PList[0].x()+a)
        
        self.PList[3]=QtCore.QPointF(self.PList[0])
        self.PList[3].setY(self.PList[0].y()+a)
        self.PList[3].setX(self.PList[0].x()+a)
        
        self.count=self.count+1
        if self.count == 1:
            self.count = 0
        
        Point0 = QtCore.QPointF(self.PList[0])
        Point1 = QtCore.QPointF(self.PList[1])
        Point2 = QtCore.QPointF(self.PList[2])
        Point3 = QtCore.QPointF(self.PList[3])
        
        Point0.setX(Point0.x()+a*(Point0.x()-xK-xKinect)/(zK+zKinect-a))
        Point0.setY(Point0.y()+a*(Point0.y()-yK-yKinect)/(zK+zKinect-a))
        
        Point1.setX(Point1.x()+a*(Point1.x()-xK-xKinect)/(zK+zKinect-a))
        Point1.setY(Point1.y()+a*(Point1.y()-yK-yKinect)/(zK+zKinect-a))
        
        Point2.setX(Point2.x()+a*(Point2.x()-xK-xKinect)/(zK+zKinect-a))
        Point2.setY(Point2.y()+a*(Point2.y()-yK-yKinect)/(zK+zKinect-a))
        
        Point3.setX(Point3.x()+a*(Point3.x()-xK-xKinect)/(zK+zKinect-a))
        Point3.setY(Point3.y()+a*(Point3.y()-yK-yKinect)/(zK+zKinect-a))
        
        self.polygon3 = QtGui.QPolygonF([Point0,Point1,self.PList[1],self.PList[0]])
        self.polygon2 = QtGui.QPolygonF([Point0,Point2,self.PList[2],self.PList[0]])
        self.polygon1 = QtGui.QPolygonF([Point0,Point1,Point3,Point2])
        self.polygon4 = QtGui.QPolygonF([Point2,Point3,self.PList[3],self.PList[2]])
        self.polygon5 = QtGui.QPolygonF([Point1,Point3,self.PList[3],self.PList[1]])
   
def main():     
   
    
    app = QtGui.QApplication(sys.argv)
    
    
    ex = CubeWidget()
    
    
    sys.exit(app.exec_())

main()
