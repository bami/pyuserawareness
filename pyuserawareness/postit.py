# -*- coding: utf-8 -*-
"""
    pyuserawareness.cube
    ~~~~~~~~~~~~~~~~~~~~

    :copyright: (c) by 2012 Oliver Schlatter <olivschl@student.ethz.ch>, Bastian Migge <miggeb@ethz.ch>
    :license: BSD3, see LICENSE for more details.
    :description: viewpoint oriented postit
    
"""


import sys
import math
import time
from PyQt4 import QtGui, QtCore
from thread import start_new_thread
from pykinecttracker.kinect import ViewpointTrackerKinect
from pykinecttracker.dummy import RandomViewpointTracker

####################################
#Notepad Information

#A quadratic sketch-area is drawn with side length s_length (in millimeter) and background color 'color'.
s_length = 300

color = 'yellow'

#With of the drawing tool
pen_with = 2
###################################

###################################

# Camera position in display coordinates [mm]
camera_position = [550.,-750.,550.]
display_orientation = 'horizontal' # or 'vertical'
#Screen Information

#In order to convert Kinect coordinates into display coordinates the pixelpitches are needed.
pixelpitch_x = 1.0 #200./45. # [pixel/mm]
pixelpitch_y = 1.0 #200./45. # [pixel/mm]

#To get the pixelpitch of a new monitor this calibration can be used. It draws a 200x200 Pixel square
#which can be measured in millimeters and the results inserted into the above constants.

Calibration = 'off' # 'on' / 'off'
###################################

###################################
#Filter Parameter
#The parameters for the nonlinear IIR extension can be set here.

#Boarder values for alpha. alpha_max > alpha_min and alpha_max, alpha_min = element of [0,1].
alpha_max = 0.994
alpha_min = 0.87

#Maximum difference between filter in- and output before the output instantly adapts the input. 
#In numbers of standard deviations. 
delta_max = 10

#Defines the slope of the drop off.
filter_b = 2.5

#Locates the drop off.
filter_c = 10

#Standard deviation values. May vary depending on the user position but are assumed constant.
#In millimeter.
stdv_x = 13
stdv_y = 5

###################################


class PostitWidget(QtGui.QWidget):
    
    def __init__(self,fileHandle=sys.stdout):
        """
            Initialize all the necessary values.
        """

        super(PostitWidget, self).__init__()
        
        self.fileHandle = fileHandle

        self.initUI()
        
        #With of the drawin tool indicator.
        self.radius = 10
        
        #List containing all the drawing point information.
        self.pointBuffer = [[]]
        
        #Drawing tool settings.
        self.pen = QtGui.QPen(QtCore.Qt.SolidLine)
        self.pen.setColor(QtCore.Qt.black)
        self.pen.setWidth(pen_with)
                        
        #Color filling settings.
        self.brush = QtGui.QBrush(QtGui.QColor(color).lighter(120))
        self.brush_pen = QtGui.QBrush(QtCore.Qt.red)
        
        #Initial viewpoint.
        #In pixel
        self.Kx = 0
        self.Ky = 0
        
        #Initial filter values.
        self.filter_x = [0,0]
        self.filter_y = [0,0]

        #Initialization of the Trafo matrix. Needed to convert the notepad's 
        #content from display to interaction coordinates.  
        self.Trafo = QtGui.QTransform(1,0,0,0,1,0,0,0,1)
        self.Inv_Trafo = QtGui.QTransform(1,0,0,0,1,0,0,0,1)
        
        #Corner Points of the notepad
        self.C = QtCore.QPointF(-s_length/2,-s_length/2)
        self.D = QtCore.QPointF(-s_length/2,s_length/2)
        self.A = QtCore.QPointF(s_length/2,s_length/2)
        self.B = QtCore.QPointF(s_length/2,-s_length/2)
        
        #Boolean value. Needed to decide whether a mouse event was onto the notepad area or not.
        self.in_pol = -1
        
        #Boolean value. Needed to decide whether a mouse event has to be drawn or not.
        self.draw_bool = -1
        
        #Rotational angle.
        #in degrees
        self.alpha = 0.
        
        #The notepad area is initialized as a polygon. 
        self.Notepad = QtGui.QPolygonF([self.A,self.B,self.C,self.D])
        self.Notepad = self.Trafo.map(self.Notepad)
        
        #Interaction point
        #in millimeter
        self.interaction_point = QtCore.QPointF(1,1)
        
        #For a touchscreen a long click is interpreted as a right click
        #otherwise all clicks are left clicks.
        self.button = 'left'
        
        
    def initUI(self): 
             
        self.text = 'Notepad App'
        
        self.setWindowTitle('Notepad Application')
        
        self.show()

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()

    def paintEvent(self, event):

        
        #######################################  
        #     VERY IMPORTANT!!!!!!            #
        #                                     #
        #The Kinect has always to be alligned #
        #parallel to the x direction of the   #
        #screen coordinates.                  #
        #######################################
        
        #List which will contain all the viewpoint information afterwards.
        self.vp = []
        
        #List index with the shortest viewpoint-interaction point distance.
        self.userIndex = 0
        
        #Initialize the viewpoint to the one of the first user which is tracked.
        closestVP = tracker.getViewpoint()
        
        #And the viewpoint-interaction point distance as well.
        closestDist = QtCore.QPointF((closestVP[0]-self.interaction_point.x()),
                               (closestVP[1]-self.interaction_point.y())).manhattanLength()
        
        numberOfViewpoints = tracker.getNumberOfUsers()
        
        #if no user can be tracked, assume the viewpoint to be at the interaction point,
        #shifted hundred pixel in positive y-direction. 
        #This results always in an upright orientation of the notepad.
        if numberOfViewpoints == 0:
            self.vp.append([self.interaction_point.x(),self.interaction_point.y()+100,1])
        
        #Check which of the tracked users is closest to the event position.
        for viewp_Ind in range(numberOfViewpoints):

            #Coordinates in Kinect workspace coordinate system
            vpCadidate = tracker.getViewpoint(viewp_Ind)
            

            if display_orientation == 'horizontal':
                #If the application is running on a table surface, use this assignment.
                vpDisplay = [vpCadidate[0],-vpCadidate[2],vpCadidate[1]]
            elif display_orientation == 'vertical': 
                #If the application is running on a vertical surface, use this assignment.
                vpDisplay = [vpCadidate[0],-vpCadidate[1],vpCadidate[2]]
            else:
                raise Exception("invalid display orientation")
            
            #Trafo from the Kinect workspace to display coordinates
            #Camera position has to be given in display coordinates in millimeter.
            vpDisplay = tracker.transformTrackingToDisplayCoordinates(viewpoint = vpDisplay,
                                    camera_position = camera_position,
                                    pixelpitch = [pixelpitch_x,pixelpitch_y,1])
            
            #But the viewpoint into the list.
            self.vp.append(vpDisplay)
            
            #Derive the distance from the user's viewpoint to the mouse event.
            dCandidate = QtCore.QPointF((vpDisplay[0]-self.interaction_point.x()),
                               (vpDisplay[1]-self.interaction_point.y())).manhattanLength()
            
            #if the user is closest to the event position, use his user index further on.
            if (dCandidate < closestDist):
                self.userIndex = viewp_Ind

        #################################
        #Nonlinear IIR filter extension.#
        #################################
        
        #Viewpoint from the last frame.
        self.filter_x[0]=self.Kx
        self.filter_y[0]=self.Ky
        
        #Unfiltered viewpoint information from the current frame (if accessible, otherwise 0)     
        self.filter_x[1] = self.vp[self.userIndex][0] if self.vp[self.userIndex][0] else 0
        self.filter_y[1] = self.vp[self.userIndex][1] if self.vp[self.userIndex][1] else 0
        
        #There are two alpha values. One for the x- and the other for the y-direction.
        alpha_x = self.alpha_func(alpha_max, alpha_min, filter_b, filter_c, 
                                  math.fabs((self.filter_x[0]-self.filter_x[1])/stdv_x))
        alpha_y = self.alpha_func(alpha_max, alpha_min, filter_b, filter_c, 
                                  math.fabs((self.filter_y[0]-self.filter_y[1])/stdv_y))
        
        #Filtered viewpoint information from the current frame.
        self.Kx = alpha_x*self.filter_x[0]+(1-alpha_x)*self.filter_x[1]
        self.Ky = alpha_y*self.filter_y[0]+(1-alpha_y)*self.filter_y[1]

        #This is an implemented form of the atan2 function:
        
        if self.interaction_point.x() == self.Kx:
            if self.interaction_point.y()< self.Ky:
                self.alpha = 0
            else:
                self.alpha = 180
        else:
            self.alpha = math.atan((self.interaction_point.y()-self.Ky)
                                   /(self.interaction_point.x()-self.Kx))*180/3.141+90
            
        if self.interaction_point.x()<self.Kx:
            self.alpha += 180

        #######
        #Paint#
        #######
        qp = QtGui.QPainter()
        qp.begin(self)
        qp.setPen(self.pen)
        
        #Calibration for the determination of the pixelpitch
        if Calibration == 'on':
            qp.drawRect(50,50,200,200)
        
        #Projection of the tracked viewpoint onto the interaction plane.
        qp.drawEllipse(QtCore.QPointF(self.Kx,self.Ky),10,10)
        
        #Check for three conditions:
        #First, has the interaction taken place in the notepad area?
        #Second, was it a left click event? (Right click resets the previously drawn information)
        #Third, is the notepad not alligned anew right now?
        #If all three are fullfilled go into drawing mode
        if (self.point_in_poly(self.interaction_point,self.Notepad) 
            and self.button=='left' and self.in_pol == 0):            
            
            qp.setBrush(self.brush) 
            
            #Draw the Notepad area and fill it with the given brush color.
            qp.drawPolygon(self.Notepad)

            #Add the current interaction point (which is inside the notepad area)
            #to the trajectory of points
            self.pointBuffer[-1].append(self.interaction_point)
            
            #Draw all the stored points.
            #The points are stored in a list of lists. Each list represents one constant drawn line.
            #After each mouse release event, the list is extended by one element.
            for draw_Ind in range(len(self.pointBuffer)):
                for p_Ind in range(len(self.pointBuffer[draw_Ind])-2):  
                    qp.drawLine(self.pointBuffer[draw_Ind][p_Ind],self.pointBuffer[draw_Ind][p_Ind+1])
                    
            qp.setBrush(self.brush_pen)

            #Just a small indicator where the event has taken place.   
            if self.draw_bool==0:
                    qp.drawEllipse(self.interaction_point.x()-self.radius,
                               self.interaction_point.y()-self.radius,
                               2*self.radius,2*self.radius)
            
            qp.setBrush(self.brush)
            
        else:
            
            #The content just gets transformed if the left button was clicked.
            #Otherwise the content just gets erased.
            if self.button=='left':
                
                self.Notepad = QtGui.QPolygonF([self.A,self.B,self.C,self.D]) 
                
                #Inverse transformation. Necessary for the new alignment since only the transformation
                #relative to the last frame is needed. 
                self.Inv_Trafo = self.Trafo.inverted()[0]

                #Get the new ABSOLUTE transformation.                                           
                self.Trafo = QtGui.QTransform(1,0,0,0,1,0,self.interaction_point.x(),
                                                       self.interaction_point.y(),1)
                self.Trafo.rotate(self.alpha)
                
                #The new RELATIVE transformation
                Trafo_rel = self.Inv_Trafo*self.Trafo
                
                self.Notepad = self.Trafo.map(self.Notepad)
            
            qp.setBrush(self.brush)
            
            qp.drawPolygon(self.Notepad)
            
            #Transform the note pad and its content with the relative transformation.
            for draw_Ind in range(len(self.pointBuffer)):
                for p_Ind in range(len(self.pointBuffer[draw_Ind])):  
                    self.pointBuffer[draw_Ind][p_Ind]=Trafo_rel.map(self.pointBuffer[draw_Ind][p_Ind])
            
            #Draw all the stored points.
            #The points are stored in a list of lists. Each list represents one constant drawn line.
            #After each mouse release event, the list is extended by one element.    
            for draw_Ind in range(len(self.pointBuffer)):
                for p_Ind in range(len(self.pointBuffer[draw_Ind])-2):  
                    qp.drawLine(self.pointBuffer[draw_Ind][p_Ind],self.pointBuffer[draw_Ind][p_Ind+1])
            
            #Boolean value to avoid drawing during the adjustment of the notepad.
            #A mouse release event is necessary to enable drawing again.   
            self.in_pol = -1
            
            qp.setBrush(self.brush) 

        #If some data has to be exported, the struct below can be used
        
        #self.data = [self.Kx,self.Ky,self.vp[self.userIndex][2],time.time()]  
        #Daten = reduce(lambda x,y: str(x)+"\t"+str(y), self.data)
        #print >>self.fileHandle ,Daten
        
        #Sleep 10 milliseconds. Helps to run at a constant frequency.
        time.sleep(0.01)
        
        #Close the paint event. 
        qp.end()
        return

    def mousePressEvent (self, event):
        """
        A mouse press event can either be with the left or the right mouse button.
        On the used touchscreen surface, a long click is interpreted as a right click.
        A normal click is interpreted as a left click.
        """

        if event.button() == QtCore.Qt.RightButton:
            self.button = 'right'
            
            #A right click erases all the drawn sketches. 
            self.pointBuffer = []
            
        
        else:
            self.button ='left'
            
            self.interaction_point = QtCore.QPointF(event.pos().x(),event.pos().y())
                        
            if len(self.pointBuffer[0])>0:
                if self.Inv_Trafo.map(self.pointBuffer[0][0]).x()==0:
                    self.pointBuffer.remove(self.pointBuffer[0][0])
        
        #If a mouse release event has occurred, nothing should be drawn. 
        #The problem is, that a mouse move event can still happen, when no button is pressed.
        #Therefore a new list appended to the pointBuffer list to avoid the connection of 
        #discontiguous points.
        if self.draw_bool == -1:
            self.pointBuffer.append([])
        
        #When a mouse press event occurred, mouse move events get again enabled
        #to add points to the sketch.    
        if event.button() == QtCore.Qt.LeftButton:
            self.draw_bool = 0
          
        #Update the graphical user interface.  
        self.update()
    
    def mouseMoveEvent (self, event):
        if self.draw_bool==0:
            self.interaction_point = QtCore.QPointF(event.pos().x(),event.pos().y())
    
        #Update the graphical user interface.  
        self.update() 
    
    def mouseReleaseEvent(self, event):
        #Boolean value, which enables drawing onto the notepad again, after a click occurred
        #outside of the drawing area.
        self.in_pol = 0
        
        #Boolean value, which prohibits drawing after a mouse release event occurred. 
        #A mouse move event can still occur when no button is pressed.
        self.draw_bool = -1
        
        self.button = 'left'
    
    def point_in_poly(self,point,poly):
        """
        A function to check whether a given point lies within a given polygon.
        Called a ray casting algorithm.
        It checks how often a ray, starting at the given point crosses the boarders of the polygon.
        If it crosses an even number, the point is outside the given polygon.
        If it crosses an odd number, the point is inside the given polygon.
        """
        x = point.x()
        y = point.y()
        n = len(poly)
        inside = False
    
        p1x,p1y = poly[0].x(),poly[0].y()
        for i in range(n+1):
            p2x,p2y = poly[i % n].x(),poly[i % n].y()
            if y > min(p1y,p2y):
                if y <= max(p1y,p2y):
                    if x <= max(p1x,p2x):
                        if p1y != p2y:
                            xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                        if p1x == p2x or x <= xinters:
                            inside = not inside
            p1x,p1y = p2x,p2y
    
        return inside 
    
    def alpha_func(self,alpha_max,alpha_min,b,c,delta_filter):
        """
        Function to determine the signal depending alpha value.
        The parameters are specified in the introduction section.
        delta_filter is in numbers of the standard deviations.
        """
        if delta_filter < delta_max:
            alpha = (alpha_max-alpha_min)/(1+math.exp(b*delta_filter-c))+alpha_min
        else:
            alpha = 0
        
        return alpha

class PostitMultiWidget(QtGui.QWidget):

    def __init__(self):
        super(PostitMultiWidget, self).__init__()

        self.initUI()

        self.x,self.y = -1,-1
        self.radius = 10

        self.pointBuffer = [[[]],[[]],[[]]]

        self.pen = QtGui.QPen(QtCore.Qt.SolidLine)
        self.pen.setColor(QtCore.Qt.black)
        self.pen.setWidth(2)

        self.Kx = 720    # Viewpoint [pixel]
        self.Ky = 450

        self.userIndex = 0

        self.brush = [QtGui.QBrush(QtCore.Qt.yellow),QtGui.QBrush(QtCore.Qt.green),QtGui.QBrush(QtGui.QColor('blue').lighter(120))]

        self.Transformation = [QtGui.QTransform(1,0,0,0,1,0,0,0,1),QtGui.QTransform(1,0,0,0,1,0,0,0,1),QtGui.QTransform(1,0,0,0,1,0,0,0,1)]
        self.Re_Transformation = [QtGui.QTransform(1,0,0,0,1,0,0,0,1),QtGui.QTransform(1,0,0,0,1,0,0,0,1),QtGui.QTransform(1,0,0,0,1,0,0,0,1)]

        self.C = QtCore.QPointF(-s_length,-s_length)
        self.D = QtCore.QPointF(-s_length,0)
        self.A = QtCore.QPointF(0,0)
        self.B = QtCore.QPointF(0,-s_length)

        self.zeichen_index = 0

        self.in_pol = -1

        self.alpha = 0.

        self.clickField = [QtGui.QPolygonF([self.A,self.B,self.C,self.D])]
        self.clickField.append(self.clickField[0])
        self.clickField.append(self.clickField[0])

        self.shiftP = QtCore.QPointF(0,0)

        self.p = QtCore.QPointF(0,0)

        self.button = 'left'


    def initUI(self):

        self.text = 'PostIt App'

        self.setWindowTitle('PostIt Application')
        self.show()

    def paintEvent(self, event):
        #self.Kx = 720+0.*math.cos(time.clock()/1.)
        #self.Ky = 450+0.*math.sin(time.clock()/1.)

        viewpoint = []
        self.clickDistance = []

        self.vp = []
        self.userIndex = 0

        closestVP = tracker.getViewpoint()
        closestDist = QtCore.QPointF((closestVP[0]-self.p.x()),
            (closestVP[1]-self.p.y())).manhattanLength()

        numberOfViewpoints = tracker.getNumberOfUsers()
        for viewpointIndex in range(numberOfViewpoints):
            vpCadidate = tracker.getViewpoint(viewpointIndex)
            #vpDisplay = [vpCadidate[0],-vpCadidate[2],vpCadidate[1]]#for table setup

            vpDisplay = [vpCadidate[0],-vpCadidate[1],vpCadidate[2]]#for upright setup
            vpDisplay = tracker.transformTrackingToDisplayCoordinates(viewpoint = vpDisplay,
                cameraPosition = [740., -360., 290.],
                pixelpitch = [pixelpitch_x,pixelpitch_y])
            self.vp.append(vpDisplay)

            dCandidate = QtCore.QPointF((vpDisplay[0]-self.p.x()),
                (vpDisplay[1]-self.p.y())).manhattanLength()
            if (dCandidate < closestDist):
                self.userIndex = viewpointIndex

        self.Kx = self.vp[self.userIndex][0] if self.vp[self.userIndex][0] else 0
        self.Ky = self.vp[self.userIndex][1] if self.vp[self.userIndex][1] else 0
        ##########################################################################################

        if self.p.x() == self.Kx:
            if self.p.y()< self.Ky:
                self.alpha = 0
            else:
                self.alpha = 180
        else:
            self.alpha = math.atan((self.p.y()-self.Ky)/(self.p.x()-self.Kx))*180/3.141+90

        if self.p.x()<self.Kx:
            self.alpha += 180

        qp = QtGui.QPainter()

        qp.begin(self)
        qp.setPen(self.pen)

        if Kalibration == 'on':
            qp.drawRect(50,50,200,200)

        qp.drawEllipse(QtCore.QPointF(self.Kx,self.Ky),10,10)

        if self.point_in_poly(self.p.x(),self.p.y(),self.clickField[self.userIndex]) and self.button=='left' and self.in_pol == 0:

            if len(self.pointBuffer[self.userIndex][-1])>1:
                Dif = QtCore.QPointF(self.pointBuffer[self.userIndex][-1][0]-self.pointBuffer[self.userIndex][-1][1])

                if Dif.manhattanLength()>40:

                    self.pointBuffer[self.userIndex][-1].remove(self.pointBuffer[self.userIndex][-1][0])


                else:
                    self.pointBuffer[self.userIndex][-1].append(self.p)

            else:
                self.pointBuffer[self.userIndex][-1].append(self.p)

            for userIndex in range(len(self.clickField)):
                qp.setBrush(self.brush[userIndex])
                qp.drawPolygon(self.clickField[userIndex])
                for zeichenIndex in range(len(self.pointBuffer[userIndex])):
                    for pointIndex in range(len(self.pointBuffer[userIndex][zeichenIndex])-2):
                        qp.drawLine(self.pointBuffer[userIndex][zeichenIndex][pointIndex],self.pointBuffer[userIndex][zeichenIndex][pointIndex+1])

            qp.setBrush(QtGui.QBrush(QtCore.Qt.red))
            qp.drawEllipse(self.p.x()-self.radius, self.p.y()-self.radius, 2*self.radius, 2*self.radius)

        else:

            if self.button=='left':

                self.clickField[self.userIndex] = QtGui.QPolygonF([self.A,self.B,self.C,self.D])

                for zeichenIndex in range(len(self.pointBuffer[self.userIndex])):
                    for pointIndex in range(len(self.pointBuffer[self.userIndex][zeichenIndex])):
                        self.Re_Transformation[self.userIndex] = self.Transformation[self.userIndex].inverted()[0]
                        self.pointBuffer[self.userIndex][zeichenIndex][pointIndex]=self.Re_Transformation[self.userIndex].map(self.pointBuffer[self.userIndex][zeichenIndex][pointIndex])

                self.Transformation[self.userIndex] = QtGui.QTransform(1,0,0,0,1,0,self.p.x(),self.p.y(),1)
                self.Transformation[self.userIndex].rotate(self.alpha)

                self.clickField[self.userIndex] = self.Transformation[self.userIndex].map(self.clickField[self.userIndex])


            self.in_pol = -1

            for zeichenIndex in range(len(self.pointBuffer[self.userIndex])):
                for pointIndex in range(len(self.pointBuffer[self.userIndex][zeichenIndex])):
                    self.pointBuffer[self.userIndex][zeichenIndex][pointIndex]=self.Transformation[self.userIndex].map(self.pointBuffer[self.userIndex][zeichenIndex][pointIndex])

            for userIndex in range(len(self.clickField)):
                qp.setBrush(self.brush[userIndex])
                qp.drawPolygon(self.clickField[userIndex])

                for zeichenIndex in range(len(self.pointBuffer[userIndex])):
                    for pointIndex in range(len(self.pointBuffer[userIndex][zeichenIndex])-2):
                        qp.drawLine(self.pointBuffer[userIndex][zeichenIndex][pointIndex],self.pointBuffer[userIndex][zeichenIndex][pointIndex+1])


        qp.end()

    def mousePressEvent (self, event):
        if event.button() == QtCore.Qt.RightButton:
            self.button = 'right'
            self.pointBuffer[self.userIndex] = [[]]
        else:
            self.button ='left'
            self.x,self.y = event.pos().x(), event.pos().y()

            self.p = QtCore.QPointF(event.pos().x(),event.pos().y())

            self.shiftP = QtCore.QPointF(self.p.x()-s_length,self.p.y()-s_length)

            if len(self.pointBuffer[self.userIndex][0])>0:
                if self.Re_Transformation[self.userIndex].map(self.pointBuffer[self.userIndex][0][0]).x()==0:
                    self.pointBuffer[self.userIndex].remove(self.pointBuffer[self.userIndex][0][0])

        self.in_pol = 0

        #if (time.clock()%12)<8:
        #    if (time.clock()%12)<4:
        #        self.userIndex = 0
        #    else:
        #        self.userIndex = 1
        #else:
        #    self.userIndex = 2


        self.update()


    def mouseMoveEvent (self, event):
        self.x,self.y = event.pos().x(), event.pos().y()
        self.p = QtCore.QPointF(event.pos().x(),event.pos().y())
        self.update()

    def mouseReleaseEvent(self, event):
        self.pointBuffer[self.userIndex].append([])
        self.in_pol = 0

    def point_in_poly(self,x,y,poly):

        n = len(poly)
        inside = False

        p1x,p1y = poly[0].x(),poly[0].y()
        for i in range(n+1):
            p2x,p2y = poly[i % n].x(),poly[i % n].y()
            if y > min(p1y,p2y):
                if y <= max(p1y,p2y):
                    if x <= max(p1x,p2x):
                        if p1y != p2y:
                            xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                        if p1x == p2x or x <= xinters:
                            inside = not inside
            p1x,p1y = p2x,p2y

        return inside

def threadLoop(widget):
    """
    If a constant running time is required, this loop can be activated.
    Should be avoided, since it forces to run the application at a very high frequency.
    """

    while(False):
        widget.update()
        time.sleep(0.01)


def run():
    #Routine to store some output data in a specified file.
    fileHandle = open(sys.argv[1],'w') if len(sys.argv) > 1 else sys.stdout
    print fileHandle

    #If no Kinect is available, the Dummytracker can be used to test the program.$
    #This Dummytracker gives some random values around the origin with a
    #maximum deviation given at the input.
    #Otherwise the normal Kinecttracker has to be used of course.
    global tracker
    #tracker = ViewpointTrackerKinect(libraryPath="./pykinecttracker/lib/libSkeletonTrackerKinect.so")
    tracker = RandomViewpointTracker(maxValue=1.0)
    
    app = QtGui.QApplication(sys.argv)
    w = PostitWidget(fileHandle=fileHandle)
    w.showMaximized()
    start_new_thread(threadLoop, (w,))
    sys.exit(app.exec_())

def runMulti():
    global tracker
    tracker = ViewpointTrackerKinect(libraryPath="./pykinecttracker/lib/libSkeletonTrackerKinect.so")
    app = QtGui.QApplication(sys.argv)

    w = PostitMultiWidget()
    w.showMaximized()
    sys.exit(app.exec_())

if __name__ == '__main__':
    run()
    
