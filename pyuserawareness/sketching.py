# -*- coding: utf-8 -*-
"""
    pyuserawareness.sketching
    ~~~~~~~~~~~~~~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge <miggeb@ethz.ch>
    :license: BSD3, see LICENSE for more details.
    :description: Simple line draw application in Python Qt4 with head tracking based
    parallax correction 
"""

import sys
import signal
from pyqtpc.gui.pyqt4.app.x11 import QApplicationPointingErrorCorrectedX11
from pykinecttracker.kinect import ViewpointTrackerKinect
from pyqtpc.correction.control.tracking import ViewpointTrackerCorrectionController
from widget.sketch import SketchWidget
from pyqtpc.correction.control.static import StaticCorrectionController

signal.signal(signal.SIGINT,signal.SIG_DFL) # Ctrl+C stops application


viewpoint_static = [100,-100]
displayOffset = 30
pixelPitch = [0.255,0.255]
cameraPosition = [205., -215., -190.]


def mainTracking():
    """ app with tracked viewpoint """
    tracker = ViewpointTrackerKinect(libraryPath="./pykinecttracker/lib/libSkeletonTrackerKinect.so")
    tracker.setSmoothingFactor(0.8)
    correction_controller = ViewpointTrackerCorrectionController(
            pixel_pitch = pixelPitch,
	        display_offset = displayOffset,
            viewpoint_tracker=tracker,
	        camera_position = cameraPosition)


    app = QApplicationPointingErrorCorrectedX11(sys.argv,correction_controller=correction_controller)

    w = SketchWidget()
    app.guiFeedback = w
    w.showMaximized()
    sys.exit(app.exec_())


def mainStatic():
    """ app with static viewpoint """

    # corrected application
    staticCorrection = StaticCorrectionController(viewpoint_static)
    app = QApplicationPointingErrorCorrectedX11(sys.argv,correction_controller=staticCorrection)

    w = SketchWidget()
    app.guiFeedback = w
    w.showMaximized()
    
    sys.exit(app.exec_())


def usage():
	print """use python pyuserawareness/sketching.py (-s|-t)

	-s static viewpoint
	-t use kinect tracker to track viewpoint
	"""


if __name__ == '__main__':

    if len(sys.argv) < 2:
	usage()
	exit(-1)
    elif sys.argv[1] == "-s":
	print "static"
    	mainStatic()
    elif sys.argv[1] == "-t":
	print "tracking"
	mainTracking()

