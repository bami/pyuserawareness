# -*- coding: utf-8 -*-
""" 
    widget/RotationButtonWidget
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~
    :copyright: (c) 2011 by Bastian Migge <miggeb@ethz.ch>
    :license: BSD3, see LICENSE for more details.
    :description: pyqt widgets 

"""

import sys
from PyQt4 import QtGui, QtCore

class AbstractClickTestWidget(QtGui.QWidget):
    """
    Widget exits on ESC
    """

    def get_activeButtonIndex(self):
        raise NotImplementedError()

    def set_activeButtonIndex(self,buttonIndex):
        raise NotImplementedError()

    activeButtonIndex = property(get_activeButtonIndex,set_activeButtonIndex)

    def get_activeButton(self):
        raise NotImplementedError()

    activeButton = property(get_activeButton)

    def updateTarget(self):
        raise NotImplementedError()

    def refreshButtons(self):
        raise NotImplementedError()

    def resizeEvent(self,event):
        self.refreshButtons()

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Escape:
            self.emit(QtCore.SIGNAL('closeEmitApp()'))
            event.accept()
        elif event.key() == QtCore.Qt.Key_Space:
            self.updateTarget()
        else:
            event.ignore()

    def mousePressEvent(self, event):
        '''
        on click handler if button passed event
        '''
        raise NotImplementedError()

class RotationButtonWidget(AbstractClickTestWidget):
    ''' widget that shows a single button. 
    
        the button moves randomly (uniform distributed) triggered by an interaction.
        the widget is shown full screen with disabled cursor feedback
        
        keys:
        * ESC closes the widget
    '''
    def get_activeButtonIndex(self):
        pass
    
    def set_activeButtonIndex(self,buttonIndex):
        pass
    
    def get_activeButton(self):
        return self.button
    
    activeButton = property(get_activeButton)

    def get_button(self):
        return self._button

    def set_button(self,button):
        # set button and connect it to call slef.updateTarget on click
        if hasattr(self, "_button") and self._button != None: 
            self._button.hide()
        
        self._button = button

        if self._button != None:
            self._button.show()
            # DO NOT USE THIS QT SIGNAL SHIT !!!! 
            # self._button.clicked.connect(self.updateTarget)
            # self.connect(self._button, QtCore.SIGNAL('LogButtonPressed'),self.updateTarget)
        
    button = property(get_button,set_button)

    def __init__(self, button, rotation=0, parent=None, logHandler=sys.stdout, 
                 considerTargetMiss=False,
                 cursor=QtCore.Qt.BlankCursor):

        self.logHandler = logHandler
        self.parent = parent
        self._considerTargetMiss = considerTargetMiss
        self.rotation = rotation
        
        QtGui.QWidget.__init__(self, parent)
        
        self.button = button 
        
        # close app signal connect (see keyPressEvent())
        self.connect(self, QtCore.SIGNAL('closeEmitApp()'), QtCore.SLOT('close()') )
        
        # widget setup
        self.setWindowTitle(str(self.__class__.__name__) + " - Hit ESC to exit")
        self.setCursor(cursor) # set cursor (e.g.QtCore.Qt.arrowCursor)
        self.showMaximized()
       
        return
    
    def __str__(self):
        return self.__class__.__name__


    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    
    mw = RotationButtonWidget(None, cursor=QtCore.Qt.ArrowCursor)
    
    mw.button = PositionRotationButton(QtGui.QPixmap("../../res/arrow.jpg"), parent=mw)
    button2 = RotationButton(QtGui.QPixmap("../../res/arrow.jpg"), parent=mw)
    
    mw.show()   
    button2.show()
    app.exec_()
