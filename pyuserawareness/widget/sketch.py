# -*- coding: utf-8 -*-
"""
    pyqtpc.gui.pyqt4.widget.sketch
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge <miggeb@ethz.ch>
    :license: BSD3, see LICENSE for more details.
    :description: Parallax corrected sketch widget
"""

from PyQt4 import QtGui, QtCore
from pyqtpc.gui import RealWorldFeedbackInterface

class SketchWidget(QtGui.QWidget, RealWorldFeedbackInterface):
    """
        Python Qt4 Sketch widget

        quits on ESC
    """
    def __init__(self, **kwargs):

        super(SketchWidget, self).__init__(**kwargs)

        self.initUI()

        self.setCursor(QtGui.QCursor(QtCore.Qt.BlankCursor))

        self._x,self._y = -1,-1
        self.radius = 10

        self.paintSwitch = 0
        self.pointBuffer = []

        self.pen = QtGui.QPen(QtCore.Qt.SolidLine)
        self.pen.setColor(QtCore.Qt.black)
        self.pen.setWidth(2)

        self.brush = QtGui.QBrush(QtCore.Qt.yellow)

    def initUI(self):

        self.text = 'drawing app'

        self.setGeometry(200, 200, 600, 400)
        self.setWindowTitle('Drawing gui')
        self.show()


    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()


    def paintEvent(self, event):
        qp = QtGui.QPainter()

        qp.begin(self)
        qp.setPen(self.pen)

        for pointIndex in range(len(self.pointBuffer)-2):
            qp.drawLine(self.pointBuffer[pointIndex],self.pointBuffer[pointIndex+1])

        if self._x > -1 and self._y > -1:
            qp.setBrush(self.brush)
            qp.drawEllipse(self._x-self.radius, self._y-self.radius, 2*self.radius, 2*self.radius)

        qp.end()

    def mousePressEvent (self, event):
        self.pointBuffer = []
        self._x,self._y = event.pos().x(), event.pos().y()
        self.update()

    def mouseMoveEvent (self, event):
        p = QtCore.QPointF(event.pos().x(),event.pos().y())
        self.pointBuffer.append(p)

        self._x,self._y = event.pos().x(), event.pos().y()

        self.update()

    def targetAt(self, position):
        """
        return self

        arguments:
            position - global window coordinates [list]
        """
        return self

    def targetFieldOfAttention(self, target):
        '''
        return center of self

        arguments
            target - [QWidget, QFrame]
        '''
        geometry = target.geometry()

        center_Qt = QtCore.QPoint(geometry.x() + geometry.width()/2., geometry.y() + geometry.height()/2.)
        center_global_Qt = self.activeWindow().mapToGlobal(center_Qt)
        center = [center_global_Qt.x(),center_global_Qt.y()]
        return center

    def targetSize(self, target):
        """
        return size of target

        arguments
            target - [QWidget, QFrame]
        '''
        """
        return [target.size().width(),target.size().height()]

    def hit(self,interaction_position, target):
        """
        returns True

        arguments:
            interaction_position - interaction coordinates [list]
            target - [QWidget,QFrame]
        """
        raise True

    def interactionError(self, interaction_position, target=None):
        """
        return the interaction error between the interaction position and the area target

        arguments:
            interaction_position - [list]
            target - [QWidget,QFrame]
        """
        return [0.,0.]
