Demo applications to show user awareness on interactive screens.

The software is based on

* [pyKinectTracker ]( https://bitbucket.org/bami/pykinecttracker )

* [kinecttracker (C++,OpenNI)]( https://bitbucket.org/bami/kinecttracker )